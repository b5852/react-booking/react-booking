//Applying bootstrap grid system
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import {Card, Button} from "react-bootstrap";


// deconstruct the "courseProp" from the props object to shorten syntax
export default function CourseCard({courseProp}){
    // Check to see if the data was passed successfully
    // Passed props is an object within an object

    // Accessing first object
    // console.log(typeof props);

    // Accessing second object
    // console.log(typeof props.courseProp);
    // console.log(props.courseProp.name);


    // Deconstruct courseProp properties into their own variable
    const {_id, name, description, price, slots} = courseProp;


    // State 
        // are used to keep track the information related to individual components

    // Hooks
        //  special react defined methods and functions that allows us to do certain tasks in our component
        // Use the state hook for this component to be able to store its state

    // Syntax 
        // const [stateName, setStateName] = useState(initialStateValue);


    // ----Activity---
    /*
        Instructions s46 Activity:
        1. Create a seats state in the CourseCard component and set the initial value to 10.
        2. For every enrollment, deduct one to the seats.
        3. If the seats reaches zero do the following:
        - Do not add to the count.
        - Do not deduct to the seats.
        - Show an alert that says No more seats available.
        5. push to git with the commit message of Add activity code - S46.
        6. Add the link in Boodle.

        const limit = 10;
        const [count, setCount] = useState(0);
        const [isOpen, setIsOpen] = useState(false)
        // console.log(useState(0))

        function enroll(){
            if (count < limit) {
                setCount(count + 1)
                console.log("Enrolees:" + count)
            }else{
                alert("No more seats");
            }
        }
    */
        // const [count, setCount] = useState(0);
        // const [seats, setSeats] = useState(5);
        // const [isOpen, setIsOpen] = useState(false);

        // function enroll(){
        //         setCount(count + 1);
        //         // console.log("Enrolees: " + count);

        //         setSeats(seats - 1);
        //         // console.log("Seats: " + seats);
        // }
        // function unenroll(){
        //     setCount(count - 1);
        // }

        // Syntax:
            // useEffect(function, [depedency])
        // useEffect(() =>{
        //     if (seats === 0) {
        //         alert("No more seats available");
        //         setIsOpen(true);
        //     }
        // }, [seats]);

        // if the useEffect() does not have a dependency array, it will run on the initial render anf whenever a satate is set by its set
        // useEffect(() =>{
        //     // Runs on every render
        //     console.log("useEffect Render")
        // })

        // If the useEffect() has a dependency array but empty it will only run on initial render
        // useEffect(() =>{
        //     // Run only on the inittial render
        //     console.log("useEffect Render")
        // }, [])


        // if the useEffect() has a dependency array 
        // useEffect(() =>{
        //     // Run only on the inittial render
        //     console.log("useEffect Render")
        // }, [seats, count])

    return(
                <Card>
                    <Card.Body>
                        <Card.Title>
                            {name}
                        </Card.Title>
                        <Card.Subtitle className="mb-2 text-muted">Description:</Card.Subtitle>
                        <Card.Text>
                            {description}
                        </Card.Text>
                        <Card.Subtitle className="mb-2 text-muted">Price:</Card.Subtitle>
                        <Card.Text>
                            {price}
                        </Card.Text>
                        <Card.Text>
                            Slots: {slots}
                        </Card.Text>
                        <Button as={Link} to={`/courses/${_id}`} variant="primary">Details</Button>
                        
                        {/*<Button variant="primary mx-1" onClick={enroll} >Enroll</Button>
                        <Button variant="danger mx-1" onClick={unenroll} >Unenroll</Button>*/}
                    </Card.Body>
                </Card>

    )
}