// Filename should be pascal case (Ex: AppNavbar.js)
// import Navbar from "react-bootstrap/Navbar"
// import Container from "react-bootstrap/Container"
// import Nav from "react-bootstrap/Nav"
// import NavDropdown from "react-bootstrap/NavDropdown"
import {useState, useEffect, useContext} from "react";
import { Link } from "react-router-dom";
import UserContext from "../UserContext"

// or use Destructuring 
import {Navbar, Nav, Container, NavDropdown} from "react-bootstrap";

export default function AppNavbar(){
	// State hook to store the information stored in th login page
	// const [user, setUser] = useState(localStorage.getItem("email"))
	const {user} = useContext(UserContext);
	console.log(user)
	return(
		<Navbar bg="light" expand="lg">
	      <Container>
	        <Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
	        <Navbar.Toggle aria-controls="basic-navbar-nav" />
	        <Navbar.Collapse id="basic-navbar-nav">
	    		{/*className is use instead of "class", to specify a CSS classes*/}
	          <Nav className="ms-auto" defaultActiveKey="/">
	            <Nav.Link as={Link} to="/" eventKey="/">Home</Nav.Link>
	            <Nav.Link as={Link} to="/courses" eventKey="/courses">Course</Nav.Link>
	            {
	            	(user.id !== null)
	            	?
	            		<Nav.Link as={Link} to="/logout" eventKey="/logout">Logout</Nav.Link>
	            	:
	            	<>
	            		<Nav.Link as={Link} to="/login" eventKey="/login">Login</Nav.Link>
	            		<Nav.Link as={Link} to="/register" eventKey="/register">Register</Nav.Link>
	            	</>
	            }
	          </Nav>
	        </Navbar.Collapse>
	      </Container>
	    </Navbar>
	)
}
