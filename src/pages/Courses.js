
import CourseCard from "../components/CourseCard";
import {useState, useEffect} from "react"
import coursesData from "../data/coursesData"

export default function Courses(){

	const [courses, setCourses] = useState([])

	useEffect(()=>{
		fetch(`http://localhost:4000/courses`)
		.then(res => res.json())
		.then(data =>{
			console.log(data)
			setCourses(data.map(course => {
				return(
					<CourseCard key={course._id} courseProp={course} />
				)
			}))
		})
	},[])

	// Check if we can access the mock database
	// console.log(coursesData);
	// console.log(coursesData[0]);

	// Props
		// is a shorthand for property since components are considered as objet in ReactJS
		// Props is a way to pass data from the parent to child component.
		// it is synonymous to the funcion parameter

		// mpa method 
		// const courses = coursesData.map(course => {
		// 	return (
		// 		<CourseCard key={course.id} courseProp={course} />
		// 	)
		// })
	return(
		<>
			<h1>Courses</h1>
			{/*<CourseCard courseProp = {coursesData[0]} />*/}
			{courses}
		</>
	)
}