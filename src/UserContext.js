// React Context
// It allows us to pass down and user (consume) data in any component we need in our React app without using props.

// It allows us to share data (state) accross component more easily.

// 3 Simple steps in using React Context
	// 1. Creating the Cotext
	// 2. Providing the context
	// 3. Consuming the context

import React from "react";

// Creating the context object
// A contaxt object as the name syate is a data type of an object that can be used to store information that can be shared to other components within the React app.
const UserContext = React.createContext();

export const UserProvider = UserContext.Provider;

export default UserContext;

